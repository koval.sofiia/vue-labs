import { createWebHistory, createRouter } from "vue-router";
import MyHome from "@/views/MyHome.vue";
import PageNotFound from "@/views/PageNotFound.vue";
import MyAbout from "@/views/MyAbout.vue";
import AirportDetail from "@/views/AirportDetail.vue";
import AirportDestinations from "@/views/AirportDestinations.vue";

const routes = [
  {
    path: "/",
    name: "MyHome",
    component: MyHome,
  },
  {
    path: "/about",
    name: "MyAbout",
    component: MyAbout,
  },
  {
    path: "/airport/:code",
    name: "AirportDetail",
    component: AirportDetail,
    children: [
      {
        path: "destinations",
        name: "AirportDestinations",
        component: AirportDestinations,
      },
    ],
  },
  {
    path: "/:catchAll(.*)*",
    name: "PageNotFound",
    component: PageNotFound,
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
