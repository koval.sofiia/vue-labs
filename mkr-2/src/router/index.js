import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import ViewAll from '@/views/ViewAll.vue'
import ViewConfig from '@/views/ViewConfig.vue'
import ViewEmployee from '@/views/ViewEmployee.vue'
const routes = [
    {
        path: '/',
        name: 'home',
        component: HomeView,
    },
    {
        path: '/employees',
        name: 'employees',
        component: ViewAll,
    },
    {
        path: '/employees/:itemId?/config',
        name: 'config',
        component: ViewConfig,
    },
    {
        path: '/employees/:itemId?/view',
        name: 'view',
        component: ViewEmployee,
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
})

export default router
