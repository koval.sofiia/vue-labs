import { createStore } from 'vuex'
import authModule from '@/store/modules/auth.js'
import usersModule from '@/store/modules/users.js'
import getModuleSettingsObject from './helpers/GetModuleSettingsObject'
export default createStore({
    namespaced: true,
    modules: {
        auth: authModule,
        recipes: getModuleSettingsObject('recipes'),
        users: usersModule,
    },
})
