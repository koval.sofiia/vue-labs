import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// Check localStorage for user data
const storedUser = JSON.parse(localStorage.getItem('user'))
if (storedUser) {
    store.commit('auth/setUser', storedUser)
}

createApp(App).use(store).use(router).mount('#app')
