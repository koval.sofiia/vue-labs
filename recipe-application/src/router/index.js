import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import ViewAll from '@/views/ViewAll.vue'
import ViewConfig from '@/views/ViewConfig.vue'
import ViewRecipe from '@/views/ViewRecipe.vue'
import LoginView from '@/views/LoginView.vue'
import store from '@/store'

const routes = [
    {
        path: '/',
        name: 'home',
        component: HomeView,
    },
    {
        path: '/about',
        name: 'about',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '@/views/AboutView.vue'),
    },
    {
        path: '/login',
        name: 'login',
        component: LoginView,
    },
    {
        path: '/recipes',
        name: 'recipes',
        component: ViewAll,
        meta: { requiresAuth: false },
    },
    {
        path: '/recipes/:itemId?/config',
        name: 'config',
        component: ViewConfig,
        meta: { requiresAuth: true },
    },
    {
        path: '/recipes/:itemId?/view',
        name: 'view',
        component: ViewRecipe,
        meta: { requiresAuth: false },
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
})

router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.requiresAuth)) {
        if (!store.getters['auth/getUser']) {
            next({ name: 'login' }) // Redirect to the login page if not authenticated
        } else {
            next()
        }
    } else {
        next()
    }
})

export default router
