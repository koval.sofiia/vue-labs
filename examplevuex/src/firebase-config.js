// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore/lite'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: 'AIzaSyByJArJG2b1U9YFsWAv4DwnZ4DP08wwRhY',
    authDomain: 'example-61087.firebaseapp.com',
    projectId: 'example-61087',
    storageBucket: 'example-61087.appspot.com',
    messagingSenderId: '391608507723',
    appId: '1:391608507723:web:f0c7d8600f3d169338dcce',
}

// Initialize Firebase
// eslint-disable-next-line no-unused-vars
const app = initializeApp(firebaseConfig)
const firebaseDB = getFirestore(app)
export default firebaseDB
