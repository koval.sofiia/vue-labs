import { createStore } from 'vuex'
import getModuleSettingsObject from './helpers/GetModuleSettingsObject'

export default createStore({
    // state: {
    //     cars: [
    //         {
    //             id: 1,
    //             name: 'Toyota Camry',
    //             factory: 'Toyota',
    //             color: 'Silver',
    //         },
    //         {
    //             id: 2,
    //             name: 'Honda Accord',
    //             factory: 'Honda',
    //             color: 'Blue',
    //         },
    //         {
    //             id: 3,
    //             name: 'Ford Mustang',
    //             factory: 'Ford',
    //             color: 'Red',
    //         },
    //         {
    //             id: 4,
    //             name: 'Chevrolet Malibu',
    //             factory: 'Chevrolet',
    //             color: 'Black',
    //         },
    //         {
    //             id: 5,
    //             name: 'Nissan Altima',
    //             factory: 'Nissan',
    //             color: 'White',
    //         },
    //     ],
    //     experts: [
    //         {
    //             id: 1,
    //             name: 'Oil Change',
    //             description: 'Replace engine oil and oil filter',
    //             assignedTo: 'Mechanic A',
    //         },
    //         {
    //             id: 2,
    //             name: 'Brake Inspection',
    //             description: 'Inspect and replace brake pads if necessary',
    //             assignedTo: 'Mechanic B',
    //         },
    //         {
    //             id: 3,
    //             name: 'Tire Rotation',
    //             description: 'Rotate tires to ensure even wear',
    //             assignedTo: 'Mechanic C',
    //         },
    //         {
    //             id: 4,
    //             name: 'Battery Check',
    //             description: 'Inspect and test the car battery',
    //             assignedTo: 'Mechanic A',
    //         },
    //         {
    //             id: 5,
    //             name: 'Air Filter Replacement',
    //             description: 'Replace the engine air filter',
    //             assignedTo: 'Mechanic B',
    //         },
    //     ],
    // },
    // getters: {
    //     getCars: ({ cars }) => cars,
    //     getCarById: (state) => (carId) => state.cars.find((c) => c.id == carId),
    //     getExperts: ({ experts }) => experts,
    //     getExpertById: (state) => (expertId) => state.experts.find((e) => e.id == expertId),
    // },
    // mutations: {
    //     removeCar(state, carIdToRemove) {
    //         state.cars = state.cars.filter((c) => c.id !== carIdToRemove)
    //     },
    //     addCar(state, carObjToAdd) {
    //         state.cars.push(carObjToAdd)
    //     },
    //     updateCar(state, carObjUpdated) {
    //         const ind = state.cars.findIndex((c) => c.id == carObjUpdated.id)
    //         state.cars[ind] = carObjUpdated
    //     },
    // },
    // actions: {
    //     deleteCarBtn({ commit }, carIdToDelete) {
    //         commit('removeCar', carIdToDelete)
    //     },
    //     addCarBtn({ commit }, carObjToCreate) {
    //         commit('addCar', carObjToCreate)
    //     },
    //     updateCarInfo({ commit }, carObjUpdatedInfo) {
    //         commit('updateCar', carObjUpdatedInfo)
    //     },
    // },
    namespaced: true,
    modules: { cars: getModuleSettingsObject('cars') },
})
