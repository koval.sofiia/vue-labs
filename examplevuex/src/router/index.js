import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import ExpertsView from '@/views/ExpertsView.vue'
import CarsView from '@/views/CarsView.vue'
import ConfigCarView from '@/views/ConfigCarView.vue'
import CreateCarView from '@/views/CreateCarView.vue'
const routes = [
    {
        path: '/',
        name: 'home',
        component: HomeView,
    },
    {
        path: '/cars',
        name: 'cars',
        component: CarsView,
    },
    {
        path: '/cars/:carId?/config',
        name: 'carConfig',
        component: ConfigCarView,
    },
    {
        path: '/cars/create',
        name: 'carCreate',
        component: CreateCarView,
    },
    {
        path: '/experts',
        name: 'experts',
        component: ExpertsView,
    },
    {
        path: '/contacts',
        name: 'contacts',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/ContactsView.vue'),
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
})

export default router
