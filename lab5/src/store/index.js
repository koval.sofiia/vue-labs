import { createStore } from 'vuex'

export default createStore({
    state: {
        lessons: [
            { code: 101, subject: 'Математика', teachers: ['Іванова', 'Петрова'] },
            { code: 102, subject: 'Фізика', teachers: ['Сидоров', 'Павлов'] },
            { code: 103, subject: 'Хімія', teachers: ['Коваленко', 'Семенова'] },
            { code: 104, subject: 'Історія', teachers: ['Мельник', 'Ковальчук'] },
            { code: 105, subject: 'Література', teachers: ['Савченко', 'Білецька'] },
            { code: 106, subject: 'Англійська мова', teachers: ['Кравець', 'Олійник'] },
        ],
        teachers: [
            { code: 1, name: 'Іванова', subject: 'Математика' },
            { code: 2, name: 'Петрова', subject: 'Математика' },
            { code: 3, name: 'Сидоров', subject: 'Фізика' },
            { code: 4, name: 'Павлов', subject: 'Фізика' },
            { code: 5, name: 'Коваленко', subject: 'Хімія' },
            { code: 6, name: 'Семенова', subject: 'Хімія' },
            { code: 7, name: 'Мельник', subject: 'Історія' },
            { code: 8, name: 'Ковальчук', subject: 'Історія' },
            { code: 9, name: 'Савченко', subject: 'Література' },
            { code: 10, name: 'Білецька', subject: 'Література' },
            { code: 11, name: 'Кравець', subject: 'Англійська мова' },
            { code: 12, name: 'Олійник', subject: 'Англійська мова' },
        ],
    },
    getters: {
        getAllLessons: (state) => state.lessons,
        getAllTeachers: (state) => state.teachers,
    },
    mutations: {},
    actions: {},
    modules: {},
})
