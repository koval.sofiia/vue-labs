import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import LessonsList from '@/components/LessonsList.vue'
import LessonsSelect from '@/views/LessonsSelect.vue'
import SignInView from '@/views/SignInView.vue'

const routes = [
    {
        path: '/',
        name: 'home',
        component: HomeView,
    },
    {
        path: '/teachers',
        name: 'teachers',
        component: () => import(/* webpackChunkName: "teachers" */ '@/components/TeachersList.vue'),
        meta: { requiresAuth: false },
    },
    {
        path: '/lessons/select',
        name: 'lessonsSelect',
        component: LessonsSelect,
        meta: { requiresAuth: true },
    },
    {
        path: '/lessons/:id(\\d*)+',
        name: 'lessons',
        component: LessonsList,
        props: true,
        meta: { requiresAuth: true },
    },
    {
        path: '/lessons/:pairs(.+)',
        name: 'listOfPairs',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "listOfPairs" */ '@/components/ListOfPairs.vue'),
        props: (route) => {
            const pairs = route.params.pairs.split('/')
            return {
                lessonTeacherPairs: pairs,
            }
        },
        meta: { requiresAuth: false },
    },
    {
        path: '/signin',
        name: 'SignIn',
        component: SignInView,
        meta: { requiresAuth: false },
    },
    {
        path: '/:pathMatch(.*)*',
        name: 'notFound',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "notFound" */ '@/components/notFound.vue'),
        meta: { requiresAuth: false },
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
})

router.beforeEach((to, from, next) => {
    const isAuthenticated = window.username !== undefined

    if (to.meta.requiresAuth && !isAuthenticated) {
        next({
            name: 'SignIn',
            query: { redirect: to.fullPath },
        })
    } else {
        next()
    }
})

export default router
