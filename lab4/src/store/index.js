import { createStore } from 'vuex'
function checkItem(item, filter) {
    for (const key in filter) {
        if (filter[key] && filter[key] !== item[key]) return false
    }
    return true
}
export default createStore({
    state: {
        items: [
            {
                id: '1',
                name: 'Spaghetti Bolognese',
                measure: 'Pasta',
                pricePerOne: '12.99',
                nationality: 'Italian',
            },
            {
                id: '2',
                name: 'Chicken Parmesan',
                measure: 'Main Course',
                pricePerOne: '15.99',
                nationality: 'Italian',
            },
            {
                id: '3',
                name: 'Vegetarian Pizza',
                measure: 'Pizza',
                pricePerOne: '10.99',
                nationality: 'Italian',
            },
            {
                id: '4',
                name: 'Caesar Salad',
                measure: 'Salad',
                pricePerOne: '8.99',
                nationality: 'Italian',
            },
            {
                id: '5',
                name: 'Grilled Salmon',
                measure: 'Seafood',
                pricePerOne: '18.99',
                nationality: 'Italian',
            },
            {
                id: '6',
                name: 'Mushroom Risotto',
                measure: 'Risotto',
                pricePerOne: '14.99',
                nationality: 'Italian',
            },
            {
                id: '7',
                name: 'BBQ Pulled Pork Sandwich',
                measure: 'Sandwich',
                pricePerOne: '11.99',
                nationality: 'Italian',
            },
            {
                id: '8',
                name: 'Tiramisu',
                measure: 'Dessert',
                pricePerOne: '7.99',
                nationality: 'Italian',
            },
            {
                id: '9',
                name: 'Vegetable Stir-Fry',
                measure: 'Vegetarian',
                pricePerOne: '13.99',
                nationality: 'Italian',
            },
            {
                id: '10',
                name: 'Chocolate Lava Cake',
                measure: 'Dessert',
                pricePerOne: '9.99',
                nationality: 'Italian',
            },
        ],
        filterObj: {},
    },
    getters: {
        getItems: ({ items, filterObj }) => items.filter((it) => checkItem(it, filterObj)),
        getItemById: (state) => (itemId) => state.items.find((it) => it.id == itemId),
    },
    mutations: {
        removeEvent(state, itemId) {
            state.items = state.items.filter((it) => it.id !== itemId)
        },
        addEvent(state, itemObj) {
            state.items.push(itemObj)
        },
        updateEvent(state, itemObj) {
            const ind = state.items.findIndex((it) => it.id == itemObj.id)
            state.items[ind] = itemObj
        },
        updateFilterEvent(state, filterObj) {
            state.filterObj = filterObj
        },
    },
    actions: {
        deleteItem({ commit }, idToRemove) {
            commit('removeEvent', idToRemove)
        },

        addItem({ commit }, itemObj) {
            commit('addEvent', { id: new Date().getTime(), ...itemObj })
        },
        updateItem({ commit }, itemObj) {
            commit('updateEvent', itemObj)
        },
        updateFilter({ commit }, filterObj) {
            commit('updateFilterEvent', filterObj)
        },
    },
    modules: {},
})
//31. Меню ресторану.
//   База даних страв:
//   назва страви,
//   одиниці вимірювання,
//   вартість одиниці,
//   страва якої кухні (національна приналежність).
//   Організувати вибір за довільним запитом.
