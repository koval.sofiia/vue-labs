import { createRouter, createWebHistory } from 'vue-router'
import ContactsView from '@/views/ContactsView.vue'
import HomeView from '@/views/HomeView.vue'
import AboutView from '@/views/AboutView.vue'
import ConfigItemView from '@/views/ConfigItemView.vue'

const routes = [
    {
        path: '/',
        name: 'home',
        component: HomeView,
    },
    {
        path: '/about',
        name: 'about',
        component: AboutView,
    },
    {
        path: '/contacts',
        name: 'contacts',
        component: ContactsView,
    },
    {
        path: '/item/:itemId?',
        name: 'config',
        component: ConfigItemView,
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
})

export default router
