import { createRouter, createWebHashHistory } from "vue-router";

import HomePage from "@/components/HomePage.vue";
import ContactPage from "@/components/ContactPage.vue";
import FriendsPage from "@/components/FriendsPage.vue";
import AddFriendPage from "@/components/AddFriendPage.vue";
import OneOfFriends from "@/components/OneOfFriends.vue";

const routes = [
  { path: "/", component: HomePage, name: "home" },
  { path: "/contacts", component: ContactPage, name: "contacts" },
  { path: "/friends", component: FriendsPage, name: "friends" },
  { path: "/friends:id?", component: OneOfFriends, name: "one-of-friends" },
  { path: "/friends/add", component: AddFriendPage, name: "add-friends" },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});
export default router;
